<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     
        $categories = [

            'rock',
            'psy',
            'indie',
            'jazz',
        ];

        foreach($categories as $category){

            DB::table('categories')->insert([

               'name' => $category,

            ]);
        }
    }
}
