<x-layout>


<div class="mb-3 offset-md-4 col-md-3 mt-5">
    <label for="exampleInputCategory" class="form-label">Categories Available:</label>
    <select name="category" class="form-control">
        @foreach($categories as $category)
        <option value="{{$category->id}}">{{$category->name}}</option>
        @endforeach
    </select>
</div>


</x-layout>