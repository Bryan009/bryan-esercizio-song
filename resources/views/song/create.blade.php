<x-layout>
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-md-12 text-center">
                <h1>Add your song to our playlist!</h1>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-md-6 offset-md-3">
                <form action="{{route('song.store')}}" method="post">
                @csrf
                   <div class="mb-3">
                        <label for="exampleInputTitle" class="form-label">Title</label>
                        <input type="text"  name="title" class="form-control" id="exampleInputTitle">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputArtist" class="form-label">Artist</label>
                        <input type="text"  name="artist" class="form-control" id="exampleInputArtist">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputGenre" class="form-label">Genre</label>
                        <input type="text"  name="genre" class="form-control" id="exampleInputGenre">
                    </div>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>
            </div>
        </div>
    </div>



</x-layout>