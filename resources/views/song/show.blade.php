<x-layout>
            
    <p>
        <button class="btn btn-primary mt-5 mx-5" type="button" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
                Change
        </button>
    </p>
    <div style="min-height: 120px;">
        <div class="collapse collapse-horizontal" id="collapseWidthExample">
            <div class="card card-body bg-dark mx-5" style="width: 400px;">
                <table class="table table-dark table-hover mt-5">
                        <tbody>
                            <tr class="text-start">
                                <th scope="row">{{$song->id}}</th>
                                <td>{{$song->title}}</td>
                                <td>{{$song->genre}}</td>
                                <td>{{$song->artist}}</td>
                            </tr>
                        </tbody>
                        <a class="btn btn-primary" href="{{route('song.index')}}">Go back</a>
                        <hr>
                        
                </table> 
            </div>
        </div>
    </div>

</x-layout>