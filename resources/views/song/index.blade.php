<x-layout>
    <div class="container-fluid col-6 offset-md-3 mt-5 bg-dark p-3">
        <table class="table table-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Artist</th>
                </tr>
        </table>
            @foreach($songs as $song)
            <table class="table table-dark table-hover">
                <tbody>
                    <tr class="text-start">
                        <th scope="row">{{$song->id}}</th>
                        <td>{{$song->title}}</td>
                        <td>{{$song->genre}}</td>
                        <td>{{$song->artist}}</td>
                    </tr>
                </tbody>
            <a class="btn btn-primary" href="{{route('song.show',compact('song'))}}"></a>
            </table> 
            @endforeach
    </div>
</x-layout>
