<x-layout>
    <div class="container-fluid">
        <div class="row p-5">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card header">
                        Song # {{$song->id}}
                    </div>

                    <card class="body">
                        <div class="row">
                            <div class="col-md-2"><h3>User</h3></div>
                            <div class="col-md-10">
                                {{$song->user->name}}
                                {{$song->user->artist}}
                                {{$song->user->title}}
                                {{$song->user->genre}}
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-2"><h3>Title</h3></div>
                            <div class="col-md-10">{{$song->title}}</div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-2"><h3>Artist</h3></div>
                            <div class="col-md-10">{{$song->artist}}</div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-2"><h3>Genre</h3></div>
                            <div class="col-md-10">{{$song->genre}}</div>
                        </div>
                    </card>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <form action="{{route('revisor.reject', $song->id)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger">
                        Reject
                    </button>
                </form>
            </div>

            <div class="col-md-6">
                <form action="{{route('revisor.accept', $song->id)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-success">
                        Accept
                    </button>
                </form>
            </div>

        </div>


    </div>
   










</x-layout>