<x-layout>
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-12 col-md-5 offset-md-3 text-center">
                <h1>Registrati</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-md-6 offset-md-4">
                 <form action="{{route('register')}}" method="post" class="col-md-6 offset col-md-3">
                    @csrf
                        <div class="mb-3">
                            <label for="exampleInputName" class="form-label">Username</label>
                            <input type="name" name="name" class="form-control" id="exampleInputName">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control" id="exampleInputPassword1">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
   


</x-layout>