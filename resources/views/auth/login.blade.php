<x-layout>
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-12 col-6 offset-md-5">
                <h1>Login</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 col-md-3 offset-md-4">
                <form action="{{route('login')}}" method="post">
                @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password"  name="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
                <br>
                <a href="{{route('register')}}" class="btn btn-success">Not registered? Do it here!</a>
            </div>
        </div>
    </div>


</x-layout>