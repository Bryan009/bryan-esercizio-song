<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
    <title>Esercizio</title>
</head>
<body>
    
<x-navbar>
</x-navbar>

{{$slot}}



<script src="{{mix('js/app.js')}}" defer></script>
</body>
</html>