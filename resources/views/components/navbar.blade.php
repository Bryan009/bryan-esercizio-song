
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand text-warning">Benvenuti sul mio sito!</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{route('homepage')}}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('song.create')}}">Upload a song</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('song.index')}}">Playlist</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('category.index')}}">Pick by category</a>
        </li>

      @guest
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Benvenuto!
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="{{route('register')}}">Register</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="{{route('login')}}">Login</a></li>
          </ul>
        </li>
      @else
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Welcome,{{Auth::user()->name}}!
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
  
      @if(Auth::user() && Auth::user()->is_revisor)
          <li class="nav-item">
            <a class="nav-link text-dark" href="{{route('revisor.home')}}">Revisor Home 
              <span class="badge badge-pill badge-danger text-danger">{{\App\Models\Song::ToBeRevisionedCount()}}</span>
            </a>
          </li>

          @else
              <li class="nav-item">
                <a href="" class="nav-link text-dark">
                  Profile
                </a>
              </li>
          @endif
          
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="" onclick="event.preventDefault(); document.querySelector('#form-logout').submit()">Logout</a></li>
          </ul>

          <form action="{{route('logout')}}" id="form-logout" method="post" style="display: none;">
            @csrf
          </form>
        </li>
      @endguest

      </ul>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>










