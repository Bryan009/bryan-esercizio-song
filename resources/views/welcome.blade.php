<x-layout>

@if (session('flash'))
    <div class="alert alert-success">
        {{ session('flash') }}
    </div>
@endif


@if (session('access.denied.revisor.only'))
    <div class="alert alert-danger">
        Access denied - Only revisors
    </div>
@endif


</x-layout>