<?php

namespace App\Models;

use App\Models\Song;
use App\Models\User;
use App\Models\Category;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Song extends Model
{
    use HasFactory;

    protected $fillable = [

        'title',
        'artist',
        'genre',
        'user_id',
        
        
    ];

    public function user(){

        return $this->belongsTo(User::class);
    }

    static public function ToBeRevisionedCount(){

        return Song::where('is_accepted', null)->count();
    }

    public function song(){

        return $this->belongsToMany(Category::class);
    }
}
