<?php

namespace App\Http\Controllers;

use App\Models\Song;
use Illuminate\Http\Request;

class RevisorController extends Controller
{
    public function __construct(){

        $this->middleware('auth.revisor');

    }

    public function index(){

        $song = Song::where('is_accepted', null)

        ->orderBy('created_at','desc')
        ->first();

        return view('revisor.home',compact('song'));

    }

    private function setAccepted($song_id,$value){

        $song = Song::find($song_id);

        $song->is_accepted = $value;
        $song->save();

        return redirect(route('revisor.home'));
    }

    public function accept($song_id){

        return $this->setAccepted($song_id,true);
    }

    public function reject($song_id){

        return $this->setAccepted($song_id,false);
    }
}
