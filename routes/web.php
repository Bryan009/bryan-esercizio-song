<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// PUBLIC CONTROLLER
Route::get('/',[PublicController::class,'homepage'])->name('homepage');

// SONG ROUTES
Route::get('/upload/song',[SongController::class,'create'])->name('song.create');
Route::post('/saving/song',[SongController::class,'store'])->name('song.store');
Route::get('/our/playlist',[SongController::class,'index'])->name('song.index');
Route::get('/check/changes/{song}',[SongController::class,'show'])->name('song.show');

// REVISOR ROUTE
Route::get('/revisor/home',[RevisorController::class,'index'])->name('revisor.home');
Route::post('revisor/song/{id}/accept',[RevisorController::class,'accept'])->name('revisor.accept');
Route::post('revisor/song/{id}/reject',[RevisorController::class,'reject'])->name('revisor.reject');

// CATEGORY ROUTE
Route::get('category/index',[CategoryController::class,'index'])->name('category.index');